package shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import shop.model.Good;
import shop.service.GoodService;

import java.util.List;

@RestController
public class GoodsController {

    @Autowired
    GoodService service;

    @GetMapping("/goods")
    public List<Good> goodsList() {
        return service.getAllGoods();
    }


    @GetMapping("/goods/{group}")
    public List<Good> listByGroup(@PathVariable("group") String group) {
        return service.getGoodsByGroup(group);
    }

}
