package shop.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import shop.controller.GoodsController;
import shop.model.User;
import shop.repository.UserRepository;

@Service
public class UserService {

    private static final Logger log = LoggerFactory.getLogger(GoodsController.class);

    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public User saveUser(String username, String password) {
        User user = new User();
        user.setUsername(username);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        user.setPassword(passwordEncoder.encode(password));
        user.setRole("ROLE_USER");
        log.info("Сохраняю базового пользователя для проверок...");
        return repository.save(user);
    }
}
