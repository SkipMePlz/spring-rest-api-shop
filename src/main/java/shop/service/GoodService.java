package shop.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shop.controller.GoodsController;
import shop.model.Good;
import shop.repository.GoodsRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GoodService {
    private static final Logger log = LoggerFactory.getLogger(GoodsController.class);
    private final GoodsRepository repository;

    public GoodService(GoodsRepository repository) {
        this.repository = repository;
    }

    public Good create(Good good) {
        log.info("Сохраняю товар: " + good.toString());
        return repository.save(good);
    }

    public Good get(int id) {
        log.info("Получаю товар по id: " + id);
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException("Нет подходящих товаров с id равным" + id));
    }

    public List<Good> getAllGoods() {
        return repository.findAll();
    }

    public List<Good> getGoodsByGroup(String group) {
        return repository.findAll().stream().filter(s -> s.getGroupName().equalsIgnoreCase(group)).collect(Collectors.toList());
    }

    public void initialize() {
        create(new Good(1, "Молоко", "Кубанская Бурёнка", randomCost(), "Молочка"));
        create(new Good(2, "Творог", "ВМК", randomCost(), "Молочка"));
        create(new Good(3, "Сыр", "ВМК", randomCost(), "Молочка"));
        create(new Good(4, "Колбаса", "Мясной дом", randomCost(), "Мясное"));
        create(new Good(5, "Молоко", "ВМК", randomCost(), "Молочка"));
        create(new Good(6, "Сок", "J-7", randomCost(), "Напитки"));
        create(new Good(7, "Cola", "Coca Cola Company", randomCost(), "Напитки"));
        create(new Good(8, "Pepsi", "Coca Cola Company", randomCost(), "Напитки"));
        create(new Good(9, "Сахар", "Русский сахар", randomCost(), "Добавки"));
        create(new Good(10, "Пирог", "Дарницкий", randomCost(), "Хлебобулочное"));
    }

    public int randomCost(){
        return 80 + (int)(Math.random() * ((160 - 80) + 1));
    }

}
