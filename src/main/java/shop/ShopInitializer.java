package shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import shop.service.GoodService;
import shop.service.UserService;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class ShopInitializer {
    @Autowired
    GoodService goodService;
    @Autowired
    UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(ShopInitializer.class, args);
    }

    @PostConstruct
    public void initialize() {
        goodService.initialize();
        userService.saveUser("user", "user");
    }
}
