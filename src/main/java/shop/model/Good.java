package shop.model;

import javax.persistence.*;

@Entity
@Table(name = "goods")
public class Good {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private String producer;
    private int cost;
    private String groupName;

    public Good() {
    }

    public Good(int id, String name, String producer, int cost, String groupName) {
        this.id = id;
        this.name = name;
        this.producer = producer;
        this.cost = cost;
        this.groupName = groupName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return "ID: " + id +
                " Наименование: " + name +
                " Производитель: " + producer +
                " Стоимость: " + cost + "руб." +
                " Группа товара: " + groupName;
    }
}
