package shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.model.Good;

public interface GoodsRepository extends JpaRepository<Good, Integer> {

}
