package shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
}
