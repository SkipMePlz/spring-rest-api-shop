FROM openjdk:8-jdk-alpine
WORKDIR /app/
COPY target/*.jar rest-api.jar
ENTRYPOINT ["java", "-jar", "rest-api.jar"]